#ifndef GAME_H
#define GAME_H

#include <QObject>
#include "story.h"
#include <QVariant>
#include <QDebug>

class Game : public QObject
{
	Q_OBJECT
public:
	explicit Game(QObject *parent = nullptr);

	void showStory(Story* s);
	Story* evaluateChoice(bool okButton);
	void processStory(bool okAnswer);
	void setEffects(possibleStory ps);
	void addGlobalEffect(QString effect, int value);
	//void findEffect(QString name);
	int random(int min, int max);
	int* getVariable(QString string);

	// player's params
	QVector<QString> globalEffectsNames = {};
	QVector<int> GlobalEffectsValues = {};

	int himHidden = 0;
	int herHidden = 0;
	int strengthHim = 10;
	int strengthHer = -10;
	int comming = -5;
	int wounded = 0;

	Story* m_currentStory;

signals:
	void setTexts(QString textField);
	void setTextFields(QVariant mainText, QVariant noButton, QVariant yesButton, QVariant debugText = "");
	void enableButtons(QVariant negButton, QVariant posButton);

public slots:
	void handleButtons(bool yesButton);
};

#endif // GAME_H
