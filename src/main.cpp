#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "game.h"
#include <QQuickView>

int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QGuiApplication app(argc, argv);
	QGuiApplication::setApplicationName("Text Editor");
	qmlRegisterType<Game>("Game", 1, 0, "Game");

	QQmlApplicationEngine engine;
	engine.load(QUrl(QLatin1String("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;


	// Connection between QT and QML
	Game w; //it should be g, but this can make it easier

	QObject *topLevel = engine.rootObjects().value(0);
	QQuickWindow *window = qobject_cast<QQuickWindow *>(topLevel);
	QObject::connect(window, SIGNAL(submitButtonPress(bool)),
						 &w, SLOT(handleButtons(bool)));

	QObject::connect(&w, SIGNAL(setTextFields(QVariant,QVariant,QVariant,QVariant)),
					 window, SLOT(setTextFields(QVariant,QVariant,QVariant,QVariant)));

	QObject::connect(&w, SIGNAL(enableButtons(QVariant,QVariant)),
					 window, SLOT(enableButtons(QVariant,QVariant)));



	// Start game

	//
	// Story init
	//

	Story* s1 = new Story();
	Story* s2 = new Story();
	Story* help1 = new Story();
	Story* help2 = new Story();
	Story* find1 = new Story();
	Story* find1alt = new Story();
	Story* find2alt = new Story();
	Story* find3alt = new Story();

	Story* find2 = new Story();
	Story* find3 = new Story();
	Story* hideHim = new Story();

	// Entered
	Story* enter = new Story();
	Story* enterCalm = new Story();
	Story* enterReady = new Story();
	Story* foundHim= new Story();
	Story* foundHer = new Story();
	Story* foundHer2 = new Story();
	Story* foundNothing = new Story();

	// Hostile action
	Story* attack = new Story();
	Story* attack2 = new Story(); // attack 2nd guy
	Story* wounded1 = new Story();
	Story* wounded2 = new Story();
	Story* himattacked = new Story();
	Story* himEscaped = new Story();

	// Results
	Story* himShot = new Story();
	Story* himNotShot = new Story();
	Story* killedFirst = new Story();
	Story* killedSecond = new Story();
	Story* herKilled = new Story();
	Story* himKilled = new Story();
	Story* himKilled1 = new Story();
	Story* herSurvived = new Story();
	Story* shotHerKilled = new Story();
	Story* shotherSurvived = new Story();


	// Intro
	w.addGlobalEffect("comming",1);

	s1->setText("You ran into house, breathing heavily, and slammed doors behind. You and wounded Catarina. But you are certain. Those raiders are still after you.",
				"Search the house", "Catarina!");
	s1->addNegOutcome(find1);
	s1->addYesOutcome(s2);

	s2->setText("Catarina has blood on her shirt. She tells you: 'Don't worry about me. We need to deal with them first.'", "Help her anyway", "Search the house");
	s2->addNegOutcome(help1,1,{"strengthHer"},{15});
	s2->addYesOutcome(find1);

	// Prepare

	help1->setText("You take her arm away from the wound. There is a small hole in her side, and blood is pouring from it. You tear your shirt and bandage the wound.",
				   "Look for things in the house", "I must hide you somewhere");
	help1->addNegOutcome(find1);
	help1->addNegOutcome(find1alt);
	help1->addNegOutcome(enter,w.comming);
	help1->addYesOutcome(help2,1,{"herHidden"},{20});
	help1->addYesOutcome(enter, w.comming,{"herHidden"},{20});

	help2->setText("You take Catarina, she embraces her hands around your neck. You carry her to a second room. After you put her down, you feel warm blood on your shirt.",
				   "Hide yourselves", "Look around for things");
	help2->addYesOutcome(find2);
	help2->addYesOutcome(find2alt);
	help2->addYesOutcome(enter, w.comming);
	help2->addNegOutcome(hideHim,1);
	help2->addNegOutcome(enter,w.comming);

	hideHim->setText("You get into dark corner, well covered from the doors. You stare at them and listen for any sounds, that raiders can be nearby.",
				   "", "Wait for those Raiders.");
	hideHim->addYesOutcome(enter,1,{"himHidden"},{10});

	find1->setText("House is small, just two rooms, deserted and messy. But there are some useful things in the end. Kitchen knife, catching rust, but it is still one sharp blade.",
				   "", "Search cubbords");
	find1->addYesOutcome(find2,1,{"strengthHim"},{10});
	find1->addYesOutcome(find2alt,1,{"strengthHim"},{10});
	find1->addYesOutcome(enter, w.comming,{"strengthHim"},{10});

	find1alt->setText("House is small, just two rooms, deserted and messy. You are searching, but there is nothing useful for defense. Everything in here is rotting.",
				   "", "Search cubbords");
	find1alt->addYesOutcome(find2);
	find1alt->addYesOutcome(find2alt);
	find1alt->addYesOutcome(enter, w.comming);

	find2->setText("You found an axe. Great!", "Return to Catarina", "Search cubbords.");
	find2->addYesOutcome(find3,1,{"strengthHim"},{10});
	find2->addYesOutcome(find3alt,1,{"strengthHim"},{10});
	find2->addYesOutcome(enter,w.comming,{"strengthHim"},{10});
	find2->addNegOutcome(help2,1,{"strengthHim"},{10});
	find2->addNegOutcome(enter,w.comming,{"strengthHim"},{10});

	find2alt->setText("Damn. Nothing useful.",
				   "Return to Catarina", "Search cubbords");
	find2alt->addYesOutcome(find3);
	find2alt->addYesOutcome(find3alt);
	find2alt->addYesOutcome(enter, w.comming * 2);
	find2alt->addNegOutcome(help2);
	find2alt->addNegOutcome(enter, w.comming);

	find3->setText("You found an old pistol. Even with magazine inside!", "", "Hide in the corner.");
	find3->addYesOutcome(hideHim,1,{"strength"},{30});
	find3->addYesOutcome(enter, w.comming,{"strength"},{30});

	find3alt->setText("You did not find a damn thing.", "", "Hide in the corner.");
	find3alt->addYesOutcome(hideHim);
	find3alt->addYesOutcome(enter, w.comming);

	// Enter
	enter->setText("You heard someone at the door. They took the handle and opened.", "", "Already...");
	enter->addYesOutcome(enterCalm, 5 + w.himHidden);
	enter->addYesOutcome(enterReady, 5);

	enterCalm->setText("Raiders entered. They don't know what's going on. They are searching the house.", "Stay hidden", "Attack");
	enterCalm->addNegOutcome(foundHer, 10 - w.herHidden);
	enterCalm->addNegOutcome(foundHim, 20 - w.himHidden);
	enterCalm->addNegOutcome(foundNothing,20,{"himHidden"},{10});
	enterCalm->addYesOutcome(attack);

	enterReady->setText("Raiders entered, ready to kill us. 'Hey, we know you here! Just crawl out, kids. We won't harm you, we promise!'", "Stay hidden", "Attack");
	enterReady->addNegOutcome(foundHer, 30 - w.herHidden);
	enterReady->addNegOutcome(foundHim, 40 - w.himHidden);
	enterReady->addNegOutcome(foundNothing,20);
	enterReady->addYesOutcome(attack);

	foundHim->setText("One of them walks into middle of the room and notices you. He yells: 'It's him! Shoot him!'", "Escape the house", "Attack");
	foundHim->addNegOutcome(himShot, 30);
	foundHim->addNegOutcome(himNotShot, 100);
	foundHim->addYesOutcome(attack);

	foundHer->setText("One of the raiders got near Catarina's hiding spot, and look in her direction. 'It's her! Hey girl, how are you?'",
					  "Stay hidden", "Attack");
	foundHer->addNegOutcome(foundHer2,1,{"herHidden, himHidden"},{-10,10});
	foundHer->addYesOutcome(attack);

	foundHer2->setText("'I'm sorry but I think I forgot a bullet inside you. Do yo think I can take it back?'",
					  "Escape the house", "Attack");
	foundHer2->addNegOutcome(himEscaped, 80);
	foundHer2->addNegOutcome(himShot, 20);
	foundHer2->addYesOutcome(attack);

	foundNothing->setText("They talk, walking around in the building.", "Stay hidden", "Attack");
	foundNothing->addNegOutcome(foundHer, 20,{"herHidden"},{-10});
	foundNothing->addNegOutcome(foundHim, 40,{"himHidden"},{-10});
	foundNothing->addNegOutcome(foundNothing,20);
	foundNothing->addYesOutcome(attack);

	// Attacking
	attack->setText("You rush from the cover and attack the first raider.", "", "Die!");
	attack->addYesOutcome(killedFirst, 50 + w.strengthHim);
	attack->addYesOutcome(wounded1, 50);
	attack->addYesOutcome(himShot, 10 - w.himHidden, {"himHidden"},{-10});

	attack2->setText("You look at the second guy. He aims his gun at you.", "Escape the house", "Attack");
	attack2->addYesOutcome(killedSecond, 20 + w.strengthHim);
	attack2->addYesOutcome(wounded2, 20,{"strengthHim"},{-10});
	attack2->addYesOutcome(himShot, 10);

	himattacked->setText("One moved close to you and spotted you. It's him! Shoot him!.", "Escape the building", "Attack");
	himattacked->addNegOutcome(himEscaped, 20);
	himattacked->addNegOutcome(himShot, 80);
	himattacked->addYesOutcome(attack, 20,{"himHidden"},{-30});

	// Results
	wounded1->setText("You jumped across the room as fast as possible, the raider was able to shoot, but you killed him with one precise blow.", "", "Die!");
	wounded1->addYesOutcome(killedFirst,1,{"strengthHim"},{-10});

	wounded2->setText("You reached for the second guy. He reacted in time, aimed his pistol at you, shot, and you felt bullet hitting you. But it did not stop you.",
					  "", "Die!");
	wounded2->addYesOutcome(killedSecond);

	// ---

	himShot->setText("You hear loud crack of a shot, and feel how piece of metal went through your body. You fall to the groud.",
					 "", "Shit...");
	himShot->addYesOutcome(himKilled);

	himNotShot->setText("You hear loud crack of a shot, but the bullet must have missed you.",
					 "Run away", "Attack!");
	himNotShot->addNegOutcome(himShot, 70);
	himNotShot->addNegOutcome(himEscaped, 30);
	himNotShot->addYesOutcome(attack);

	killedFirst->setText("You hit the raider into chest. He gasped and fell on the floor.","Escape the house", "Kill the other one");
	killedFirst->addNegOutcome(himEscaped, 30);
	killedFirst->addNegOutcome(himShot, 70);
	killedFirst->addYesOutcome(attack2, 40);

	killedSecond->setText("The second man was hit in his throat. Blood spilled everywhere, and it took a long time before he stopped moving, holding his bleeding throat.","", "Katarina!");
	killedSecond->addYesOutcome(herKilled, 5 - w.wounded*100);
	killedSecond->addYesOutcome(herSurvived, 30 + w.strengthHer - w.wounded*100);
	killedSecond->addYesOutcome(shotherSurvived, 100 * w.wounded * 6);
	killedSecond->addYesOutcome(shotHerKilled, 100 * w.wounded);

	// Epilogues
	himKilled->setText("'What about the girl?' said one raider, somewhere in the distance. 'We can still have fun, right?'. 'True. But not with this guy.'",
					   "Motherfuckers", "Please, spare her...");
	himKilled->addNegOutcome(himKilled1);
	himKilled->addYesOutcome(himKilled1);

	herKilled->setText("You get to her, see her pale face, lips slowly moving, eyes wide open. And blood. \n \
					Lot of blood aroud. Please... don't be dead. She smiles, weakly, and whispers: 'Live... Nelson.'","", "");
	herSurvived->setText("Katarina! You get to her, she is shaken, but allright.\n \
					'Everyting is allright, love. We're safe.' She gives you a big, warm kiss. 'I love you, Nelson.'","", "");
	shotherSurvived->setText("'Are you okay, Katarina?' You ask her, but your feet does not support you as well as you wanted.\n \
							'Nelson!' she cries. 'It will be alright,' you tell her. But the room darkens.\n \
							You lie your head in her lap, world gets darker. But you have her. Everything will be allright.","", "");
	shotHerKilled->setText("'Are you okay, Katarina?' You ask her, but your feet does not support you as well as you wanted.\n \
						   She does not answer. You see a lot of blood around you, maybe some is yours, rest is from Catarina. The word is shading out, you feel the shock is getting you slowly but surely. \n \
							You embrace her, for the last time, as you are feeling more and more dizzy. And black embraces all.","", "");

	himKilled1->setText("'You hear what he said?' said one raider. 'He still breathes!' laughed another. 'But not for long!' You feel cold metal at your head.\n \
						And Catarina, looking at your last moment. And then the sound of a gunshot.", "", "");

	himEscaped->setText("Run. Run your life. You made it from the house. Then you hear loud scream. And two shots.\n \
						Scream stopped. But you run, do not stop. Her voice and gun cracks burned into your mind. Catarina.", "", "");


	// NOTE: Final story - jenom obrzek / text, zadny vyber
	// NOTE: New game jako option? (nebo decision na konci - end, begin)

	// Story init end

	w.showStory(s1);


	return app.exec();
}
