#include "story.h"

Story::Story() {}
Story::~Story() {}

void Story::setText(QString textField,
					 QString noText,
					 QString okText)
{
	this->textField = textField;
	this->okText = okText;
	this->noText = noText;
}

void Story::addYesOutcome(Story* s,
						  int chance,
						  QVector<QString> effects,
						  QVector<int> vals)
{
	this->addOutcome(true, s, chance, effects, vals);
}
void Story::addNegOutcome(Story* s,
						  int chance,
						  QVector<QString> effects,
						  QVector<int> vals)
{
	this->addOutcome(false, s, chance, effects, vals);
}

void Story::addOutcome(bool yesOutcome,
					   Story* s,
					   int chance,
					   QVector<QString> effects,
					   QVector<int> values)
{
	possibleStory collection;
	collection.story = s;
	collection.chance = chance;
	if (effects.size() > 0)
	{
		collection.effects = effects;
		collection.values = values;
	}
	else
	{
		collection.effects = {};
		collection.values = {};
	}

	if (yesOutcome)
		this->okStories.push_back(collection);
	else
		this->noStories.push_back(collection);
}
