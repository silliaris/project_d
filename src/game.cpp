#include "game.h"

Game::Game(QObject *parent) : QObject(parent)
{

}

void Game::	showStory(Story* s)
{
	emit setTextFields(s->textField, s->noText, s->okText, "");

	// Disable buttons that does not lead to any story
	// TODO: If chance of all stories is 0, do the same(or chance of the story is 0, right?)

	QVariant negButton = true;
	QVariant yesButton = true;
	if (s->noStories.length() < 1)
		negButton = false;
	if (s->okStories.length() < 1)
		yesButton = false;

	emit enableButtons(negButton, yesButton);

	this->m_currentStory = s;
}

int Game::random(int min, int max)
{
		return qrand() % ((max + 1) - min) + min;
}

void Game::processStory(bool okAnswer)
{
	Story* newStory = this->evaluateChoice(okAnswer);
	this->showStory(newStory);
}

// Calculate what story should be next, and what effects should be applied.
Story* Game::evaluateChoice(bool okButton)
{
	Story* story = this->m_currentStory;

	// find out branch to be evaluated
	QVector<possibleStory> stories = story->okStories;

	if (!okButton)
		stories = story->noStories;

	int chancesSum = 0;
	foreach(possibleStory i, stories)
	{
		// Chance must not be lesser than 0
		i.chance = qMax(0,i.chance);
		chancesSum += i.chance;
	}

	int rand = this->random(1,chancesSum);

	// Find out number it belongs to
	Story* selectedStory = nullptr;
	possibleStory selectedChoice;
	int min = 0;
	int max = 0;

	foreach(possibleStory i, stories)
	{
		max = min + i.chance;
		if (rand > min && rand <= max)
		{
			selectedChoice = i;
			selectedStory = i.story;
			break;
		}
		else
		{
			min = max;
		}
	}

	// Prepare effects of the branch
	this->setEffects(selectedChoice);

	QString message = "himH:" + QString::number(this->himHidden) +
			"|herH:" + QString::number(this->herHidden) +
			"|str:" + QString::number(this->strengthHim) +
			"|strHer:" + QString::number(this->strengthHer) +
			"|wound:" + QString::number(this->wounded) +
			"|comming:" + QString::number(this->comming);
	//statusBar()->showMessage(message);

	return selectedStory;
}

void Game::setEffects(possibleStory ps)
{
	// Get local and global effects together

	QVector<QString> effects = this->globalEffectsNames;
	QVector<int> values = this->GlobalEffectsValues;
	effects.append(ps.effects);
	values.append(ps.values);

	for (int i=0; i < effects.size(); i++)
	{
		int* var = this->getVariable(effects[i]);
		if (var != nullptr)
			*var += values[i];
	}
}

int* Game::getVariable(QString string)
{
	int* var = nullptr;
	if (string == "himHidden")
		var = &this->himHidden;
	else if (string == "herHidden")
		var = &this->herHidden;
	else if (string == "strength")
		var = &this->strengthHim;
	else if (string == "comming")
		var = &this->comming;
	else if (string == "strengthHer")
		var = &this->strengthHer;
	else if (string == "wounded")
		var = &this->wounded;
	else
	{
		printf("Incorrect variable");
		var = nullptr; // error value
	}
	return var;
}

void Game::addGlobalEffect(QString effect, int value)
{
	this->globalEffectsNames.push_back(effect);
	this->GlobalEffectsValues.push_back(value);
}

void Game::handleButtons(bool yesButton)
{
	qDebug() << "c++: Comm::handleButtons:" << yesButton;

	Story* selectedStory = this->evaluateChoice(yesButton);
	this->showStory(selectedStory);

	//process decision
	// give me a new story

	//emit setTextFields("AAAAAAAAAAAAA!!!!","","","");

}

