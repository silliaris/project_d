#ifndef STORY_H
#define STORY_H

#include <QObject>
#include <QVector>
//#include "mainwindow.h"

class Story;

struct effect {
	QString type;
	int value;
};

struct possibleStory {
	Story* story;
	int chance;
	QVector<QString> effects;
	QVector<int> values;
};

class Story
{
private:
public:
	Story();
	~Story();

	// Variables
	QString textField;
	QString okText;
	QString noText;

	QVector<possibleStory> okStories = QVector<possibleStory>();
	QVector<possibleStory> noStories = QVector<possibleStory>();

	// Story operations
	void setText(QString textField = "", QString noText = "", QString okText = "");
	void addOutcome(bool yesOutcome,
					Story* s,
					int chance = 1,
					QVector<QString> effects = QVector<QString>(),
					QVector<int> values = QVector<int>());
	void addYesOutcome(Story* s,
							 int chance = 1,
							 QVector<QString> effects = {},
							 QVector<int> values = {});
	void addNegOutcome(Story* s,
							 int chance = 1,
							 QVector<QString> effects = {},
							 QVector<int> values = {});
};


#endif // STORY_H
