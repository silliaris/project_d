import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import Game 1.0
import QtQuick.Window 2.2 // Window.FullScreen


//import QtQuick.Controls.Material 2.1

ApplicationWindow {
    visible: true
    //visibility: Window.FullScreen
    width: 400
    height: 600
    title: qsTr("Project D: Android Version")
    //Material.theme: Material.Dark
    //Material.accent: Material.DeepPurple

    signal submitButtonPress(bool yesButton)

    // this function is our QML slot
    function setTextFields(mainText, negText, yesText, debText){
        console.log("setTextField: " + mainText)
        textField.text = mainText
        debugText.text = debText
        negButton.text = negText
        posButton.text = yesText
    }

    function enableButtons(noButton, yesButton){
        console.log("buttons enabled: " + yesButton + noButton)
        negButton.enabled = noButton
        posButton.enabled = yesButton
    }

    ColumnLayout {
        id: columnLayout
        z: 1
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent


        Text {
            id: debugText
            color: "#ffffff"
            //color: "#b0bec5"
            text: qsTr("This is just debug text field, for providing debugInfo where it makes sense")
            horizontalAlignment: Text.AlignHCenter
            leftPadding: 5
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            font.pixelSize: 12
        }

        Text {
            id: textField
            color: "#ffffff"
            //color: "#B0BEC5"
            text: qsTr("Just plain text, nothing extra")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.minimumWidth: 1
            elide: Text.ElideLeft
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            font.pixelSize: 20
        }

        RowLayout {
            id: rowLayout
            width: 100
            height: 100
            transformOrigin: Item.Bottom
            spacing: 5
            Layout.fillHeight: false
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom

            Button {
                id: negButton
                text: qsTr("False Button")
                Layout.fillHeight: false
                Layout.fillWidth: true
                onClicked: {
                    submitButtonPress(false)
                    //textField.text = "Nope";
                }
            }

            Button {
                id: posButton
                text: qsTr("True Button")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.fillWidth: true
                onClicked: {
                    submitButtonPress(true)
                    //posButton.text = "Yelp";
                }
            }
        }


    }

    Image {
        id: image
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        source: "test2.png"
    }

}
